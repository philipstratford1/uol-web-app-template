# Buttons
Buttons should be styled so as to provide the user with visual cues as to the flow they are expected to take through the application.

## Primary button
The standard button style.

<div class='markup-example'>
<div class='example-snippet'>
<button class='btn btn-primary'>Primary</button>
</div>
<div class='code-snippet'>
````
<button class='btn btn-primary'>Primary</button>
````
</div>
</div>

## Action button
Action buttons are used to signify the most prominent call to action on a page. There should usually only be one Action button on a page.

<div class='markup-example'>
<div class='example-snippet'>
<button class='btn btn-primary btn-lg'>Action</button>
</div>
<div class='code-snippet'>
````
<button class='btn btn-primary btn-lg'>Action</button>
````
</div>
</div>

## Secondary and tertiary buttons
Secondary and tertiary buttons are used to compliment the primary button when not all actions are of equal importance. These different button colours may also be used when the primary blue colour does not match the context that they are used.

<div class='markup-example'>
<div class='example-snippet'>
<button class='btn btn-secondary'>Secondary</button>
<button class='btn btn-warning'>Tertiary 1</button>
<button class='btn btn-success'>Tertiary 2</button>
</div>
<div class='code-snippet'>
````
<button class='btn btn-secondary'>Secondary</button>
<button class='btn btn-warning'>Tertiary 1</button>
<button class='btn btn-success'>Tertiary 2</button>
````
</div>
</div>
