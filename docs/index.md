# Web App Template 
Testing 456.

This is what some code could look like:
````
public string TestString(string prefix)
{
    return prefix + " - ta da!"
}
````

## Our Own Styles
The big question is can we use our own stylesheet with our code examples?

<div class='markup-example'>
<div class='example-snippet'>
<button class='btn btn-primary'>Test</button>
</div>
<div class='code-snippet'>
````
<button class='btn btn-primary'>Test</button>
````
</div>
</div>
